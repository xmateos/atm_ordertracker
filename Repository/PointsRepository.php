<?php

namespace ATM\OrderTrackerBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PointsRepository extends EntityRepository{

    public function mediaHasPoints($mediaId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('p')
            ->from('ATMOrderTrackerBundle:Points','p')
            ->join('p.media','m','WITH',$qb->expr()->eq('m.id',$mediaId));

        return is_null($qb->getQuery()->getOneOrNullResult()) ? false : $qb->getQuery()->getOneOrNullResult();
    }

    public function getUserTotalPoints($userId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('SUM(p.points) as total')
            ->from('ATMOrderTrackerBundle:Points','p')
            ->join('p.media','m')
            ->join('m.owner','u','WITH',$qb->expr()->eq('u.id',$userId))
            ->where(
                $qb->expr()->eq('p.isPaid',0)
            );

        $result = $qb->getQuery()->getArrayResult();

        return $result[0]['total'];
    }
}