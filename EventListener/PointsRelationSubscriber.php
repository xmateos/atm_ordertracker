<?php

namespace ATM\OrderTrackerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class PointsRelationSubscriber implements EventSubscriber
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\OrderTrackerBundle\Entity\Points') {
            return;
        }

        $metadata->mapOneToOne(array(
            'targetEntity' => $this->config['media'],
            'fieldName' => 'media',
            'joinColumns' => array(
                array(
                    'name' => 'media_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));
    }
}