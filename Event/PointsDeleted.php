<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PointsDeleted extends Event{

    const NAME = 'atm_order_tracker_points_deleted.event';

    protected $pointsDeleted;
    protected $media;

    public function __construct($pointsDeleted,$media)
    {
        $this->pointsDeleted = $pointsDeleted;
        $this->media = $media;
    }

    public function getPointsDeleted()
    {
        return $this->pointsDeleted;
    }

    public function setPointsDeleted($pointsDeleted)
    {
        $this->pointsDeleted = $pointsDeleted;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
    }
}