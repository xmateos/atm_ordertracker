<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AfterProductGifted extends Event{
    const NAME = 'atm_order_tracker_after_product_gifted.event';
    private $purchase;
    private $message;
    private $user;

    public function __construct($user,$purchase,$message)
    {
        $this->user = $user;
        $this->purchase = $purchase;
        $this->message = $message;
    }

    public function getPurchase()
    {
        return $this->purchase;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }
}