<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class StockEmpty extends Event{
    const NAME = 'atm_order_tracker_stock_empty.event';

    private $product_name;
    private $size_name;

    public function __construct($product_name,$size_name)
    {
        $this->product_name = $product_name;
        $this->size_name = $size_name;
    }

    public function getProductName()
    {
        return $this->product_name;
    }

    public function setProductName($product_name)
    {
        $this->product_name = $product_name;
    }

    public function getSizeName()
    {
        return $this->size_name;
    }

    public function setSizeName($size_name)
    {
        $this->size_name = $size_name;
    }
}