<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PointsAdded extends Event{

    const NAME = 'atm_order_tracker_points_added.event';

    protected $point;
    protected $media;

    public function __construct($point,$media)
    {
        $this->point = $point;
        $this->media = $media;
    }

    public function getPoint()
    {
        return $this->point;
    }

    public function setPoint($point)
    {
        $this->point = $point;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
    }
}