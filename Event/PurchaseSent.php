<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PurchaseSent extends Event{
    const NAME = 'atm_order_tracker_purchase_sent.event';
    private $purchase;
    private $user;

    public function __construct($user,$purchase)
    {
        $this->user = $user;
        $this->purchase = $purchase;
    }

    public function getPurchase()
    {
        return $this->purchase;
    }

    public function getUser()
    {
        return $this->user;
    }
}