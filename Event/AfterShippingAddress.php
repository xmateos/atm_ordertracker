<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AfterShippingAddress extends Event{
    const NAME = 'atm_order_tracker_after_shipping_address.event';
    private $shippingAddress;
    private $user;

    public function __construct($user,$shippingAddress)
    {
        $this->user = $user;
        $this->shippingAddress = $shippingAddress;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function getUser()
    {
        return $this->user;
    }
}