<?php

namespace ATM\OrderTrackerBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PointsUpdated extends Event{

    const NAME = 'atm_order_tracker_points_updated.event';

    protected $point;
    protected $pointsBeforeUpdate;
    protected $media;

    public function __construct($point,$pointsBeforeUpdate,$media)
    {
        $this->point = $point;
        $this->pointsBeforeUpdate = $pointsBeforeUpdate;
        $this->media = $media;
    }

    public function getPoint()
    {
        return $this->point;
    }

    public function setPoint($point)
    {
        $this->point = $point;
    }

    public function getPointsBeforeUpdate()
    {
        return $this->pointsBeforeUpdate;
    }

    public function setPointsBeforeUpdate($pointsBeforeUpdate)
    {
        $this->pointsBeforeUpdate = $pointsBeforeUpdate;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
    }
}