<?php

namespace ATM\OrderTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\OrderTrackerBundle\Entity\Product;
use ATM\OrderTrackerBundle\Entity\Stock;
use ATM\OrderTrackerBundle\Entity\Image;
use ATM\OrderTrackerBundle\Entity\PurchaseProductSize;
use ATM\OrderTrackerBundle\Entity\Purchase;
use ATM\OrderTrackerBundle\Event\AfterPurchase;
use ATM\OrderTrackerBundle\Event\StockEmpty;
use ATM\OrderTrackerBundle\Form\ProductType;
use ATM\OrderTrackerBundle\Form\PurchaseType;
use ATM\OrderTrackerBundle\Services\SearchProducts;
use \DateTime;

class ProductController extends Controller{
    /**
     * @Route("/create/product", name="atm_ot_create_product")
     */
    public function createProductAction(){
        $product = new Product();

        $form = $this->createForm(ProductType::class,$product);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $this->getParameter('atm_order_tracker_config');
            if(!is_dir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'])){
                mkdir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder']);
            }
            $arrProduct = $request->get('product');
            $init_date = DateTime::createFromFormat('d/m/Y',$arrProduct['init_date']);
            $end_date = DateTime::createFromFormat('d/m/Y',$arrProduct['end_date']);
            $product->setInitDate($init_date);
            $product->setEndDate($end_date);

            $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/product_images';
            if(!is_dir($pathToFolder)){
                mkdir($pathToFolder);
            }

            $fileFront = $request->files->get('flImageFront');
            $filename = md5(uniqid()).'.'.$fileFront->guessExtension();
            $fileFront->move($pathToFolder,$filename);

            $imageFront = new Image();
            $imageFront->setIsMain(true);
            $imageFront->setProduct($product);
            $imageFront->setPath($config['media_folder'].'/product_images/'.$filename);
            $imageFront->setPosition(1);

            $fileBack = $request->files->get('flImageBack');
            if(!is_null($fileBack)){
                $filename = md5(uniqid()).'.'.$fileBack->guessExtension();
                $fileBack->move($pathToFolder,$filename);

                $imageBack = new Image();
                $imageBack->setIsMain(true);
                $imageBack->setProduct($product);
                $imageBack->setPath($config['media_folder'].'/product_images/'.$filename);
                $imageBack->setPosition(2);
                $em->persist($imageBack);
            }

            $em->persist($product);
            $em->persist($imageFront);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_ot_stock',array('productId'=>$product->getId())));
        }


        return $this->render('ATMOrderTrackerBundle:Product:create.html.twig',array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/products/list/{page}", name="atm_ot_product_list", defaults={"page":1})
     */
    public function productListAction($page){

        $params = array(
            'show_out_of_stock' => true
        );
        $products = $this->get(SearchProducts::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Product:list.html.twig',array(
            'products' => $products['results'],
            'pagination' => $products['pagination']
        ));
    }

    /**
     * @Route("/delete/product/{productId}", name="atm_ot_product_delete")
     */
    public function deleteProductAction($productId){
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ATMOrderTrackerBundle:Product')->findOneById($productId);

        foreach($product->getImages() as $image){
            @unlink($this->get('kernel')->getRootDir().'/../web/'.$image->getPath());
        }

        $em->remove($product);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_ot_product_list'));
    }

    /**
     * @Route("/edit/product/{productId}", name="atm_ot_edit_product")
     */
    public function editProductAction($productId){
        $config = $this->getParameter('atm_order_tracker_config');
        $rootDir = $this->get('kernel')->getRootDir().'/../web/';

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ATMOrderTrackerBundle:Product')->findOneById($productId);

        $form = $this->createForm(ProductType::class,$product);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $fileFront = $request->files->get('flImageFront');

            if(!is_null($fileFront)){
                $imageFrontOld = $product->getImages(1);

                if(!is_null($imageFrontOld)){
                    $pathToImageFront = $rootDir.$imageFrontOld->getPath();
                    @unlink($pathToImageFront);
                    $em->remove($imageFrontOld);
                    $em->flush();
                }

                $imageFront = new Image();
                $imageFront->setIsMain(true);
                $imageFront->setProduct($product);
                $imageFront->setPosition(1);
                $filenameFront = md5(uniqid()).'.'.$fileFront->guessExtension();
                $fileFront->move($rootDir.$config['media_folder'].'/product_images',$filenameFront);
                $imageFront->setPath($config['media_folder'].'/product_images/'.$filenameFront);

                $em->persist($imageFront);
            }

            $fileBack = $request->files->get('flImageBack');
            if(!is_null($fileBack)){
                $imageBackOld = $product->getImages(2);
                if(!is_null($imageBackOld)){
                    $pathToImageBack = $rootDir.$imageBackOld->getPath();
                    @unlink($pathToImageBack);
                    $em->remove($imageBackOld);
                    $em->flush();
                }

                $imageBack = new Image();
                $imageBack->setIsMain(true);
                $imageBack->setProduct($product);
                $imageBack->setPosition(2);
                $filenameBack = md5(uniqid()).'.'.$fileBack->guessExtension();
                $fileBack->move($rootDir.$config['media_folder'].'/product_images',$filenameBack);
                $imageBack->setPath($config['media_folder'].'/product_images/'.$filenameBack);

                $em->persist($imageBack);
            }

            $stocks = $em->getRepository('ATMOrderTrackerBundle:Stock')->findBy(array('product'=>$productId));
            foreach($stocks as $stock){

                $found = false;
                foreach($product->getSizes() as $size){
                    if($size->getId() == $stock->getSize()->getId()){
                        $found = true;
                        break;
                    }
                }

                if(!$found){
                    $em->remove($stock);
                }
            }

            $em->persist($product);
            $em->flush();
            return new RedirectResponse($this->get('router')->generate('atm_ot_product_list'));
        }

        $arrImages = array();
        foreach($product->getImages() as $image){
            if($image->getPosition() == 1){
                $arrImages['front'] = $image;
            }elseif($image->getPosition() == 2){
                $arrImages['back'] = $image;
            }else{
                $arrImages[] = $image;
            }
        }

        return $this->render('ATMOrderTrackerBundle:Product:edit.html.twig',array(
            'product' => $product,
            'arrImages' => $arrImages,
            'form' => $form->createView()
        ));
    }

    /**
    * @Route("/product/purchase/{max_results}/{page}", name="atm_ot_product_purchase", defaults={"max_results":10,"page":1})
    */
    public function seeProductsAction($max_results,$page){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $config = $this->getParameter('atm_order_tracker_config');
        $currentDate = new DateTime();

        $paramsProducts = array(
            'date' => $currentDate->format('Y-m-d'),
            'page' => $page,
            'pagination' => true,
            'max_results' => $max_results
        );
        $products = $this->get(SearchProducts::class)->search($paramsProducts);

        /*$qbPurchase = $em->createQueryBuilder();
        $qbPurchase
            ->select('p')
            ->from('ATMOrderTrackerBundle:Purchase','p')
            ->join('p.user','u','WITH',$qbPurchase->expr()->eq('u.id',$user->getId()))
            ->orderBy('p.creationDate','DESC');
        $lastPurchase = $qbPurchase->getQuery()->getResult();*/
        /*if(isset($lastPurchase[0])){
            $purchase = $lastPurchase[0];
        }else{
            $purchase = new Purchase();
        }
        $form = $this->createForm(PurchaseType::class,$purchase);*/

        return $this->render('ATMOrderTrackerBundle:Purchase:products.html.twig',array(
            'products' => $products['results'],
            'pagination' => $products['pagination'],
            'points' => $user->getPoints(),
            'max_results' => $max_results,
            //'purchase_form' => $form->createView(),
            'minimum_points' => $config['minimum_points_for_purchase'],
            'minimum_product_tokens_for_adding_to_wishlist' => $config['minimum_product_tokens_for_adding_to_wishlist']
        ));
    }

    /**
     * @Route("/product/purchase/page/{max_results}/{page}", name="atm_ot_product_purchase_page", defaults={"max_results":10,"page":1} ,options={"expose"=true})
     */
    public function productPurchasePageAction($max_results,$page){
        $currentDate = new DateTime();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $config = $this->getParameter('atm_order_tracker_config');

        $params = array(
            'date' => $currentDate->format('Y-m-d'),
            'page' => $page,
            'pagination' => true,
            'max_results' => $max_results
        );

        $products = $this->get(SearchProducts::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Purchase:products_list.html.twig',array(
            'products' => $products['results'],
            'pagination'=> $products['pagination'],
            'points' => $user->getPoints(),
            'minimum_points' => $config['minimum_points_for_purchase']
        ));
    }

    /**
     * @Route("/store/purchase/session/{productId}/{sizeId}/{pointsToSpend}/{pointsSpent}", name="atm_ot_store_purchase", options={"expose"=true})
     */
    public function storePurchaseInSessionAction($productId,$sizeId,$pointsToSpend,$pointsSpent){
        $session = $this->get('session');

        if(!$session->has('user_purchases')){
            $session->set('user_purchases',array());
            $session->set('user_purchases_points',array());
        }

        $userPurchases = $session->get('user_purchases');

        if(array_key_exists($productId,$userPurchases)){
            unset($userPurchases[$productId]);
            $session->set('user_purchases',$userPurchases);
        }else{
            $userPurchases[$productId] = $sizeId;
            $session->set('user_purchases',$userPurchases);
        }

        $userPurchasesPoints = array(
            'points_to_spend' => $pointsToSpend,
            'points_spent' => $pointsSpent
        );
        $session->set('user_purchases_points',$userPurchasesPoints);

        return new Response('ok');
    }

    /**
     * @Route("/do/purchase", name="atm_ot_do_purchase")
     */
    public function doPurchaseAction(){
        $session = $this->get('session');
        if($session->has('user_purchases')){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();
            $request = $this->get('request_stack')->getCurrentRequest();

            $purchase = new Purchase();
            $form = $this->createForm(PurchaseType::class,$purchase);

            $purchasedProducts = $session->get('user_purchases');
            $productIds = array();
            foreach($purchasedProducts as $productId => $sizeId){
                $productIds[] = $productId;
            }
            $products = $this->get(SearchProducts::class)->search(array(
                'ids' => $productIds,
                'pagination' => null,
            ));

            if($request->getMethod() == 'POST'){

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {

                    $total = 0;
                    foreach($products['results'] as $product){

                        $purchaseProductSize = new PurchaseProductSize();
                        $purchaseProductSize->setPrice($product['price']);
                        $purchaseProductSize->setSize(null);
                        $purchaseProductSize->setPurchase($purchase);

                        $sizeId = null;
                        $sizeName = null;
                        if(!is_null($purchasedProducts[$product['id']])){
                            $size = $em->getRepository('ATMOrderTrackerBundle:Size')->findOneById($purchasedProducts[$product['id']]);

                            $purchaseProductSize->setSize($size);
                            $sizeId = $size->getId();
                            $sizeName = $size->getName();
                        }
                        $purchaseProductSize->setProductName($product['name']);
                        $purchase->addPurchasedProductSizes($purchaseProductSize);

                        $total+= $product['price'];
                        $em->persist($purchaseProductSize);

                        if(!is_null($sizeId)){
                            $stock = $em->getRepository('ATMOrderTrackerBundle:Stock')->findOneBy(array('product'=>$product['id'],'size'=>$sizeId));
                            $stock->decreaseStock();
                            $em->persist($stock);

                            if($stock->getStock() == 0){
                                $stockEmptyEvent = new StockEmpty($product['name'],$sizeName);
                                $this->get('event_dispatcher')->dispatch(StockEmpty::NAME, $stockEmptyEvent );
                            }
                        }
                    }

                    $purchase->setTotal($total);
                    $purchase->setUser($user);

                    $em->persist($purchase);

                    $userPoints = $user->getPoints();
                    $user->setPoints($userPoints - $total);
                    $em->persist($user);

                    $em->flush();

                    $session->remove('user_purchases');
                    $session->remove('user_purchases_points');

                    $event = new AfterPurchase($user,$purchase);
                    $this->get('event_dispatcher')->dispatch(AfterPurchase::NAME, $event);

                    $config = $this->getParameter('atm_order_tracker_config');
                    return new RedirectResponse($this->get('router')->generate($config['route_after_purchase']));
                }
            }else{

                $shippingAddress = $em->getRepository('ATMOrderTrackerBundle:ShippingAddress')->findOneBy(array('user'=> $user->getId()));

                return $this->render('ATMOrderTrackerBundle:Purchase:purchase.html.twig',array(
                    'products' => $products['results'],
                    'purchasedProducts' => $purchasedProducts,
                    'purchase_form' => $form->createView(),
                    'shippingAddress' => $shippingAddress
                ));
            }
        }else{
            return new RedirectResponse($this->get('router')->generate('atm_ot_product_purchase'));
        }
    }

    /**
     * @Route("/get/country/code/{countryId}", name="atm_ot_country_code", options={"expose"=true})
     */
    public function getCountryPhoneCodeAction($countryId){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial c.{id,phoneCode}')
            ->from('ATMOrderTrackerBundle:Country','c')
            ->where($qb->expr()->eq('c.id',$countryId));

        $country = $qb->getQuery()->getArrayResult();

        return new Response('+'.$country[0]['phoneCode']);
    }

    /**
     * @Route("/set/stock/{productId}", name="atm_ot_stock")
     */
    public function setStockAction($productId){
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository('ATMOrderTrackerBundle:Product')->findOneById($productId);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $stocks = $request->get('stock');

            foreach($stocks as $sizeId => $amount){
                $stock = $em->getRepository('ATMOrderTrackerBundle:Stock')->findOneBy(array('size'=>$sizeId,'product'=>$productId));

                if(is_null($stock)){
                    $size = $em->getRepository('ATMOrderTrackerBundle:Size')->findOneById($sizeId);

                    $stock = new Stock();
                    $stock->setSize($size);
                    $stock->setProduct($product);
                }
                $stock->setStock($amount);

                $em->persist($stock);
            }
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_ot_product_list'));
        }

        $stocks = $em->getRepository('ATMOrderTrackerBundle:Stock')->findBy(array('product'=>$productId));

        return $this->render('ATMOrderTrackerBundle:Product:stock.html.twig',array(
            'product' => $product,
            'stocks' => $stocks
        ));
    }

    /**
     * @Route("/save/product/positions/{positions}", name="atm_ot_save_product_positions", options={"expose"=true})
     */
    public function savePositionsAction($positions){
        $em = $this->getDoctrine()->getManager();
        $positions = json_decode($positions,true);

        foreach($positions as $productId => $position){
            $product = $em->getRepository('ATMOrderTrackerBundle:Product')->findOneById($productId);
            $product->setPosition($position);
            $em->persist($product);
        }

        $em->flush();

        return new Response('ok');
    }
}