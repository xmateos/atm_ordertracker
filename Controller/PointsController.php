<?php

namespace ATM\OrderTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\OrderTrackerBundle\Entity\Points;
use ATM\OrderTrackerBundle\Event\PointsAdded;
use ATM\OrderTrackerBundle\Event\PointsUpdated;
use ATM\OrderTrackerBundle\Event\PointsDeleted;
use ATM\OrderTrackerBundle\Services\SearchPoints;
use \DateTime;

class PointsController extends Controller{
    const STATUS_READY = 0;
    const STATUS_DRAFT = 3;

    /**
     * @Route("/points/set/{mediaId}/{points}", name="atm_ot_set_points", options={"expose"=true})
     */
    public function setPointsAction($mediaId,$points){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $point = $em->getRepository('ATMOrderTrackerBundle:Points')->mediaHasPoints($mediaId);
        $media = $em->getRepository($config['media'])->findOneById($mediaId);

        if(!$point){
            $point = new Points();
            $point->setMedia($media);
            $point->setPoints($points);

            $em->persist($point);
            $em->flush();

            $event = new PointsAdded($point, $media);
            $this->get('event_dispatcher')->dispatch(PointsAdded::NAME, $event);

            return new Response('ok');
        }else{
            $pointsBeforeUpdated = $point->getPoints();

            $point->setPoints($points);

            $em->persist($point);
            $em->flush();

            $event = new PointsUpdated($point,floatval($pointsBeforeUpdated), $media);
            $this->get('event_dispatcher')->dispatch(PointsUpdated::NAME, $event);

            return new Response('updated');
        }
    }



    /**
     * @Route("/{userId}", name="atm_ot_points_index",defaults={"userId":null}, options={"expose"=true})
     */
    public function indexAction($userId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $qbUsers = $em->createQueryBuilder();

        $qbUsers
            ->select('u')
            ->from($config['user'],'u')
            ->orderBy('u.username','ASC')
        ;

        $orX = $qbUsers->expr()->orX();
        foreach($config['user_media_roles'] as $role){
            $orX->add($qbUsers->expr()->like('u.roles',$qbUsers->expr()->literal('%'.$role.'%')));
        }

        $qbUsers->where($orX);
        $qbUsers->andWhere(
            $qbUsers->expr()->andX(
                $qbUsers->expr()->eq('u.enabled',1),
                $qbUsers->expr()->eq('u.locked',0)
            )
        );

        $users = $qbUsers->getQuery()->getArrayResult();

        return $this->render('ATMOrderTrackerBundle:Points:index.html.twig',array(
            'users' => $users,
            'userId' => $userId
        ));
    }

    /**
     * @Route("/get/user/media/{userId}", name="atm_ot_points_get_user_media", options={"expose"=true} )
     */
    public function getUserMedia($userId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');
        $currentDate = new DateTime('+1 day');

        $qbPoints = $em->createQueryBuilder();
        $qbPoints
            ->select('p.id')
            ->from('ATMOrderTrackerBundle:Points','p')
            ->join('p.media','p_m')
            ->where($qbPoints->expr()->eq('p_m.id','m.id'));

        $qbMediaImages = $em->createQueryBuilder();
        $qbMediaImages
            ->select('m')
            ->from($config['media'],'m')
            ->join('m.'.$config['user_field_name'],'u','WITH',$qbMediaImages->expr()->eq('u.id',$userId))
            ->where(
                $qbMediaImages->expr()->andX(
                    $qbMediaImages->expr()->lte('m.publishdate',$qbMediaImages->expr()->literal($currentDate->format('Y-m-d'))),
                    $qbMediaImages->expr()->eq('m.hidden',0),
                    $qbMediaImages->expr()->neq('m.status',self::STATUS_DRAFT),
                    $qbMediaImages->expr()->notIn('m.id',$qbPoints->getQuery()->getDQL()),
                    $qbMediaImages->expr()->orX(
                        $qbMediaImages->expr()->eq('m.content_type',SearchPoints::CONTENT_TYPE_IMAGE),
                        $qbMediaImages->expr()->eq('m.content_type',SearchPoints::CONTENT_TYPE_GIF)
                    )
                )
            )
        ;
        $mediaImages = $qbMediaImages->getQuery()->getArrayResult();


        $qbMediaVideos = $em->createQueryBuilder();
        $qbMediaVideos
            ->select('m')
            ->from($config['media'],'m')
            ->join('m.'.$config['user_field_name'],'u','WITH',$qbMediaVideos->expr()->eq('u.id',$userId))
            ->where(
                $qbMediaVideos->expr()->andX(
                    $qbMediaVideos->expr()->lte('m.publishdate',$qbMediaVideos->expr()->literal($currentDate->format('Y-m-d'))),
                    $qbMediaVideos->expr()->eq('m.hidden',0),
                    $qbMediaVideos->expr()->eq('m.status',self::STATUS_READY),
                    $qbMediaVideos->expr()->notIn('m.id',$qbPoints->getQuery()->getDQL()),
                    $qbMediaVideos->expr()->eq('m.content_type',SearchPoints::CONTENT_TYPE_VIDEO)
                )
            )
        ;
        $mediaVideos = $qbMediaVideos->getQuery()->getArrayResult();


        $htmlVideos = '<option value="--">Select a video</option>';
        foreach($mediaVideos as $video){
            $htmlVideos .= '<option value="'.$video['id'].'">'.$video['title'].'</option>';
        }

        $htmlGalleries = '<option value="--">Select a gallery</option>';
        foreach($mediaImages as $image){
            $htmlGalleries .= '<option value="'.$image['id'].'">'.$image['title'].'</option>';
        }

        return new Response(json_encode(
            array(
                'videos' => $htmlVideos,
                'galleries' => $htmlGalleries
            )
        ));
    }

    /**
     * @Route("/submit/add/points", name="atm_ot_points_submit_add_points")
     */
    public function submitAddPoints(){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');
        $request = $this->get('request_stack')->getCurrentRequest();

        $galleryId = $request->get('galleryId');
        $videoId = $request->get('videoId');
        $pointsSet = $request->get('points');
        $date = $request->get('date');
        $dateTime = DateTime::createFromFormat('d-m-Y', $date);

        $mediaId = $videoId != '--' ? $videoId : $galleryId;
        $media = $em->getRepository($config['media'])->findOneById($mediaId);

        $points = new Points();
        $points->setPoints($pointsSet);
        $points->setMedia($media);
        $points->setCreationDate($dateTime);

        $em->persist($points);
        $em->flush();

        $event = new PointsAdded($points, $media);
        $this->get('event_dispatcher')->dispatch(PointsAdded::NAME, $event);

        return new RedirectResponse($this->get('router')->generate('atm_ot_points_index'));
    }

    /**
     * @Route("/points/image/table/{userId}/{page}", name="atm_ot_points_images_table",defaults={"userId":"--","page":1}, options={"expose"=true})
     */
    public function imagesTableAction($userId,$page){
        $params = array(
            'max_results'=> 10,
            'pagination'=> true,
            'content_type' => array(
                SearchPoints::CONTENT_TYPE_GIF,
                SearchPoints::CONTENT_TYPE_IMAGE,
            ),
            'user_id' => $userId != '--' ? $userId : null,
            'page' => $page
        );
        $points = $this->get(SearchPoints::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Points:imagesTable.html.twig',array(
            'points' => $points['results'],
            'pagination' => $points['pagination'],
            'config' => $this->getParameter('atm_order_tracker_config')
        ));
    }

    /**
     * @Route("/points/video/table/{userId}/{page}", name="atm_ot_points_videos_table",defaults={"userId":"--","page":1}, options={"expose"=true})
     */
    public function videosTableAction($userId,$page){
        $params = array(
            'max_results'=> 10,
            'pagination'=> true,
            'content_type' => array(
                SearchPoints::CONTENT_TYPE_VIDEO,
            ),
            'user_id' => $userId != '--' ? $userId : null,
            'page' => $page
        );

        $points = $this->get(SearchPoints::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Points:videosTable.html.twig',array(
            'points' => $points['results'],
            'pagination' => $points['pagination'],
            'config' => $this->getParameter('atm_order_tracker_config')
        ));
    }

    /**
     * @Route("/points/delete/{pointsId}", name="atm_ot_delete_points", options={"expose"=true})
     */
    public function deletePointsAction($pointsId){
        $em = $this->getDoctrine()->getManager();

        $points = $em->getRepository('ATMOrderTrackerBundle:Points')->findOneById($pointsId);
        $media = $points->getMedia();

        $pointsToDiscount = $points->getPoints();

        $em->remove($points);
        $em->flush();

        $event = new PointsDeleted($pointsToDiscount,$media);
        $this->get('event_dispatcher')->dispatch(PointsDeleted::NAME, $event);

        return new Response('ok');
    }
}