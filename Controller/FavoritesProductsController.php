<?php

namespace ATM\OrderTrackerBundle\Controller;

use ATM\OrderTrackerBundle\Entity\ShippingAddress;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\OrderTrackerBundle\Entity\FavoriteProduct;
use ATM\OrderTrackerBundle\Entity\Purchase;
use ATM\OrderTrackerBundle\Entity\PurchaseProductSize;
use ATM\OrderTrackerBundle\Form\ShippingAddressType;
use ATM\OrderTrackerBundle\Event\StockEmpty;
use ATM\OrderTrackerBundle\Event\AfterProductGifted;
use ATM\OrderTrackerBundle\Event\AfterShippingAddress;

class FavoritesProductsController extends Controller{

    /**
     * @Route("/set/shipping/address", name="atm_ot_favorite_product_shipping_address")
     */
    public function setShippingAddressAction(){
        $shippingAddress = new ShippingAddress();
        $form = $this->createForm(ShippingAddressType::class,$shippingAddress);
        $request = $this->get('request_stack')->getCurrentRequest();

        if($request->getMethod() == 'POST') {

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {

                $user = $this->get('security.token_storage')->getToken()->getUser();
                $em = $this->getDoctrine()->getManager();

                $shippingAddress->setUser($user);
                $em->persist($shippingAddress);
                $em->flush();

                $event = new AfterShippingAddress($user,$shippingAddress);
                $this->get('event_dispatcher')->dispatch(AfterShippingAddress::NAME, $event);

                $this->get('session')->getFlashBag()->add('atm_ot_shipping_address_set','Thank you for submitting your address.<br><br>Your wish list is now ready and you can select items from the store by clicking the "star" icon. Once clicked, the item will appear on your wish list on your profile where fans can easily send you gifts. For any questions, please reach out to <a href="mailto:ibi@bikinifanatics.com">ibi@bikinifanatics.com</a>');

                return $this->redirect($this->get('router')->generate('atm_ot_product_purchase'));
            }
        }

        return $this->render('ATMOrderTrackerBundle:FavoriteProduct:shipping_address.html.twig',array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/approve/shipping/address/{shipping_address_id}", name="atm_ot_favorite_product_approve_shipping_address")
     */
    public function approveShippingAddressAction($shipping_address_id){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $shippingAddress = $em->getRepository('ATMOrderTrackerBundle:ShippingAddress')->findOneById($shipping_address_id);
        $shippingAddress->setApproved(true);
        $em->persist($shippingAddress);
        $em->flush();

        $this->get('session')->getFlashBag()->add('atm_ot_shipping_address_approved','Shipping address approved for the user: '.$shippingAddress->getUser()->getUsername());
        return $this->redirect($this->get('router')->generate($config['route_after_purchase']));
    }

    /**
     * @Route("/add/remove/{productId}/{sizeId}", name="atm_ot_add_remove_favorite_product", options={"expose"=true})
     */
    public function addRemoveFavoriteProductAction($productId,$sizeId){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial f.{id}')
            ->from('ATMOrderTrackerBundle:FavoriteProduct','f')
            ->join('f.user','u','WITH',$qb->expr()->eq('u.id',$user->getId()))
            ->join('f.product','p','WITH',$qb->expr()->eq('p.id',$productId));

        $favoriteProduct = $qb->getQuery()->getResult();

        if(empty($favoriteProduct)){
            $product = $em->getRepository('ATMOrderTrackerBundle:Product')->findOneById($productId);
            $size = $em->getRepository('ATMOrderTrackerBundle:Size')->findOneById($sizeId);

            $favoriteProduct = new FavoriteProduct();
            $favoriteProduct->setProduct($product);
            $favoriteProduct->setSize($size);
            $favoriteProduct->setUser($user);

            $em->persist($favoriteProduct);
            $em->flush();

            $code = 'add';
            $message = 'Product added to your favorite products list.';
            $title = 'Remove product from favorites';
        }else{
            $em->remove($favoriteProduct[0]);
            $em->flush();

            $code = 'remove';
            $message = 'Product removed from your favorite products list.';
            $title = 'Add product to favorites';
        }

        return new JsonResponse(array('code'=>$code,'message'=>$message,'title'=>$title));
    }

    /**
     * @Route("/see/{usernameCanonical}/{page}", name="atm_ot_favorite_products_overview", defaults={"page":1} )
     */
    public function favoriteProductsAction($usernameCanonical,$page){
        $config = $this->getParameter('atm_order_tracker_config');
        $em = $this->getDoctrine()->getManager();
        $ownerProfile = $em->getRepository($config['user'])->findOneBy(array('usernameCanonical' => $usernameCanonical));
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $isOwnerProfile = $user->getUsernameCanonical() == $usernameCanonical ? true : false;

        if($user->getId() != $ownerProfile->getId() && $user->hasRole('ROLE_MODEL')){
            return $this->redirect($this->get('router')->generate('atm_ot_favorite_products_overview',array(
                'usernameCanonical' => $user->getUsernameCanonical()
            )));
        }

        $qbIds = $em->createQueryBuilder();
        $qbIds
            ->select('partial fp.{id}')
            ->from('ATMOrderTrackerBundle:FavoriteProduct','fp')
            ->join('fp.user','u','WITH',$qbIds->expr()->eq('u.id',$ownerProfile->getId()))
            ->orderBy('fp.creation_date','DESC');

        if(!$isOwnerProfile){
            $qbIds
                ->join('fp.product','p')
                ->join('p.stocks','st','WITH',$qbIds->expr()->gt('st.stock',0))
                ->groupBy('p.id')
            ;
        }

        $favoriteProductsIds = $qbIds->getQuery()->getArrayResult();

        $favoriteProducts = $pagination = null;
        if(!empty($favoriteProductsIds)){

            $favoriteProductsIds = array_map(function($fp){
                return $fp['id'];
            },$favoriteProductsIds);

            $pagination = $this->get('knp_paginator')->paginate(
                $favoriteProductsIds,
                $page,
                8
            );

            $ids = $pagination->getItems();


            $qb = $em->createQueryBuilder();
            $qb
                ->select('partial fp.{id,creation_date}')
                ->addSelect('partial p.{id,name,canonical,price,description,tokens}')
                ->addSelect('partial pi.{id,path,isMain}')
                ->addSelect('partial s.{id,name}')
                ->addSelect('partial st.{id,stock}')
                ->addSelect('partial st_size.{id,name}')
                ->from('ATMOrderTrackerBundle:FavoriteProduct','fp')
                ->join('fp.product','p')
                ->join('p.stocks','st')
                ->join('st.size','st_size')
                ->join('p.images','pi')
                ->join('fp.size','s')
                ->where($qb->expr()->in('fp.id',$ids))
                ->orderBy('fp.creation_date','DESC');

            $favoriteProducts = $qb->getQuery()->getArrayResult();
        }

        return $this->render('ATMOrderTrackerBundle:FavoriteProduct:list.html.twig',array(
            'page' => $page,
            'pagination'=> $pagination,
            'favoritesProducts' => $favoriteProducts,
            'profile' => $ownerProfile
        ));
    }

    /**
     * @Route("/send/gift", name="atm_ot_favorite_products_send_gift")
     */
    public function sendGiftAction(){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $request = $this->get('request_stack')->getCurrentRequest();
        $favorite_product_id = $request->get('fav_product_id');
        $anonymousGift = $request->get('anonymous') == 1 ? true : false;
        $message = $request->get('msg_model');

        $favoriteProduct = $em->getRepository('ATMOrderTrackerBundle:FavoriteProduct')->findOneById($favorite_product_id);
        $ownerProfile = $favoriteProduct->getUser();
        $product = $favoriteProduct->getProduct();
        $size = $favoriteProduct->getSize();

        if($product->getTokens() > $user->getTokens()){
            $this->get('request_stack')->getCurrentRequest()->getSession()->getFlashBag()->add('notEnoughTokens','You don\'t have enough tokens for purchasing this gift.');
            return new RedirectResponse($this->get('router')->generate('atm_ot_favorite_products_overview',array(
                'usernameCanonical' => $ownerProfile->getUsernameCanonical(),
                'page' => 1
            )));
        }

        $tokens = $user->getTokens();
        $tokens -= $product->getTokens();
        $user->setTokens($tokens);
        $em->persist($user);

        $shippingAddress = $em->getRepository('ATMOrderTrackerBundle:ShippingAddress')->findOneBy(array('user'=>$ownerProfile->getId()));

        $purchase = new Purchase();
        $purchase->setUser($user);
        $purchase->setEmail($ownerProfile->getEmail());
        $purchase->setAddress($shippingAddress->getAddress());
        $purchase->setCity($shippingAddress->getCity());
        $purchase->setState($shippingAddress->getState() ? $shippingAddress->getState(): null);
        $purchase->setZipCode($shippingAddress->getZipCode());
        $purchase->setCountry($shippingAddress->getCountry() ? $shippingAddress->getCountry(): null);
        $purchase->setFirstName($shippingAddress->getFirstName());
        $purchase->setLastName($shippingAddress->getLastName() ? $shippingAddress->getLastName() : null);
        $purchase->setDeliveryInstructions($shippingAddress->getDeliveryInstructions() ? $shippingAddress->getDeliveryInstructions() : null);
        $purchase->setPhoneNumber($shippingAddress->getPhone() ? $shippingAddress->getPhone() : null);
        $purchase->setTotal($product->getTokens());
        $purchase->setGiftedTo($ownerProfile);
        $purchase->setAnonymousGift($anonymousGift);
        $em->persist($purchase);

        $purchaseProductSize = new PurchaseProductSize();
        $purchaseProductSize->setProductName($product->getName());
        $purchaseProductSize->setPurchase($purchase);
        $purchaseProductSize->setSize($size);
        $purchaseProductSize->setTokens($product->getTokens());
        $em->persist($purchaseProductSize);

        $stock = $em->getRepository('ATMOrderTrackerBundle:Stock')->findOneBy(array('product'=>$product->getId(),'size'=>$size->getId()));
        $stock->decreaseStock();
        $em->persist($stock);

        $em->remove($favoriteProduct);

        $em->flush();

        if($stock->getStock() == 0){
            $stockEmptyEvent = new StockEmpty($product->getName(),$size->getName());
            $this->get('event_dispatcher')->dispatch(StockEmpty::NAME, $stockEmptyEvent );
        }

        $event = new AfterProductGifted($user,$purchase,$message);
        $this->get('event_dispatcher')->dispatch(AfterProductGifted::NAME, $event);

        $this->get('session')->getFlashBag()->add('atm_ot_product_gifted','Thank you for your gift, we will process it as soon as possible!');

        return $this->redirect($this->get('router')->generate('atm_ot_favorite_products_overview',array(
            'usernameCanonical' => $ownerProfile->getUsernameCanonical()
        )));
    }
}