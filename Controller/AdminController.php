<?php

namespace ATM\OrderTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\OrderTrackerBundle\Entity\Image;
use ATM\OrderTrackerBundle\Services\SearchPurchases;
use ATM\OrderTrackerBundle\Event\PurchaseSent;
use ATM\OrderTrackerBundle\Form\ShippingAddressType;
use \DateTime;

class AdminController extends Controller
{
    /**
     * @Route("/list/{initDate}/{endDate}/{sent}/{onlyGifts}/{page}", name="atm_ot_index", defaults={"page":1,"sent":0, "onlyGifts":0}, options={"expose"=true})
     */
    public function indexAction($initDate,$endDate,$sent, $onlyGifts,$page){

        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial u.{id,username}')
            ->from($config['user'],'u');
        if(!empty($config['user_roles'])){
            $orX = $qb->expr()->orX();
            foreach($config['user_roles'] as $role)
            {
                $orX->add($qb->expr()->like('u.roles',$qb->expr()->literal('%'.$role.'%')));
            }
            $qb->where($orX);
        }

        $qb->orderBy('u.username');

        $dates = $this->getAdminDates($initDate,$endDate);


        return $this->render('ATMOrderTrackerBundle:Admin:index.html.twig',array(
            'yearsFilter' => $dates['years'],
            'initDateTokens' => $dates['initDateTokens'],
            'endDateTokens' => $dates['endDateTokens'],
            'initDate' => $initDate,
            'endDate' => $endDate,
            'initPeriod' => $dates['initPeriod'],
            'endPeriod' => $dates['endPeriod'],
            'page' => $page,
            'sent' => $sent,
            'users' => $qb->getQuery()->getArrayResult(),
            'onlyGifts' => $onlyGifts
        ));
    }

    private function getAdminDates($initDate,$endDate)
    {
        $initYear = 2017;
        $currentDate = new DateTime();
        $currentYear = $currentDate->format('Y');

        $years = array();
        for ($year = $initYear; $year <= $currentYear; $year++) {
            $years[] = $year;
        }

        $initDateTokens = explode('-', $initDate);
        $endDateTokens = explode('-', $endDate);

        $initPeriod = DateTime::createFromFormat('Y-m-d', $initDate);
        $endPeriod = DateTime::createFromFormat('Y-m-d', $endDate);

        return array(
            'years' => $years,
            'initDateTokens' => $initDateTokens,
            'endDateTokens' => $endDateTokens,
            'initPeriod' => $initPeriod,
            'endPeriod' => $endPeriod
        );
    }

    public function searchPurchasesAction($initDate = null,$endDate = null,$sent = null,$onlyGifts = null, $giftedTo = null,$page = 1,$userId = null){
        $params = array(
            'date_range' => array(
                'init_date' => $initDate,
                'end_date' => $endDate
            ),
            'page' => $page,
            'pagination' => true,
            'max_results' => 10,
            'user_ids' => !is_null($userId) ? array(intval($userId)) : null,
            'gifted_to' => $giftedTo,
            'show_only_gifts' => $onlyGifts
        );


        if(!is_null($sent)){
            $sent = $sent == 1 ? true : false;
            $params['sent'] = $sent;
            $params['order_by_field'] = 'sendDate';
        }

        $purchases = $this->get(SearchPurchases::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Admin:purchases_table.html.twig',array(
            'purchases' => $purchases['results'],
            'pagination' => $purchases['pagination'],
            'sent' =>$sent
        ));
    }

    /**
     * @Route("/purchase/details/{purchaseId}", name="atm_ot_purchase_details", options={"expose"=true})
     */
    public function getPurchaseDetailsAction($purchaseId){

        $params = array(
            'ids' => array($purchaseId)
        );

        $purchase = $this->get(SearchPurchases::class)->search($params);

        return $this->render('ATMOrderTrackerBundle:Admin:purchase_details.html.twig',array(
            'purchase' => $purchase['results'][0]
        ));
    }

    /**
     * @Route("/purchase/set/send/{purchaseId}", name="atm_ot_purchase_set_send", options={"expose"=true})
     */
    public function setPurchaseAsSentAction($purchaseId){
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request_stack')->getCurrentRequest();
        $sendDate = $request->get('txSendDate');
        $sendDate = DateTime::createFromFormat('d-m-Y',$sendDate);
        $config = $this->getParameter('atm_order_tracker_config');

        if(!is_dir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'])){
            mkdir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder']);
        }

        $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/purchases_images';
        if(!is_dir($pathToFolder)){
            mkdir($pathToFolder,0777);
        }

        $file = $request->files->get('purchaseImage');
        $filename = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($pathToFolder,$filename);

        $image = new Image();
        $image->setIsMain(true);
        $image->setPath($config['media_folder'].'/purchases_images/'.$filename);
        $em->persist($image);

        $purchase = $em->getRepository('ATMOrderTrackerBundle:Purchase')->findOneById($purchaseId);
        $purchase->setImage($image);
        $purchase->setSent(true);
        $purchase->setSendDate($sendDate);
        $em->persist($purchase);
        $em->flush();

        $event = new PurchaseSent($purchase->getUser(),$purchase);
        $this->get('event_dispatcher')->dispatch(PurchaseSent::NAME, $event);

        $redirectUrl = $request->get('url');

        return new RedirectResponse($redirectUrl);
    }

    /**
     * @Route("/historical/{userId}/{giftedTo}/{page}", name="atm_ot_historical", options={"expose"=true},defaults={"page":1, "giftedTo": null})
     */
    public function historicalAction($userId,$giftedTo,$page){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial u.{id,username}')
            ->from($config['user'],'u')
            ->orderBy('u.username');

        $currentDate = new DateTime();
        $currentYear = $currentDate->format('Y');
        $currentMonth = $currentDate->format('m');
        $initDate = $currentYear.'-'.$currentMonth.'-01';
        $numDays = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
        $endDate = $currentYear.'-'.$currentMonth.'-'.$numDays;

        $dates = $this->getAdminDates($initDate,$endDate);



        return $this->render('ATMOrderTrackerBundle:Admin:historical.html.twig',array(
            'yearsFilter' => $dates['years'],
            'initDateTokens' => $dates['initDateTokens'],
            'endDateTokens' => $dates['endDateTokens'],
            'initDate' => $initDate,
            'endDate' => $endDate,
            'initPeriod' => $dates['initPeriod'],
            'endPeriod' => $dates['endPeriod'],
            'page' => $page,
            'users' => $qb->getQuery()->getArrayResult(),
            'userId' => $userId,
            'sent' => 0,
            'giftedTo' => $giftedTo
        ));
    }

    /**
     * @Route("/manage/countries/{letter}", name="atm_ot_manage_countries",defaults={"letter":"A"})
     */
    public function manageCountriesAction($letter){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial c.{id,name,code,phoneCode,hidden}')
            ->from('ATMOrderTrackerBundle:Country','c')
            ->where($qb->expr()->like('c.name',$qb->expr()->literal($letter.'%')))
            ->orderBy('c.name','ASC');

        return $this->render('ATMOrderTrackerBundle:Admin:countries.html.twig',array(
            'letter' => $letter,
            'countries' => $qb->getQuery()->getArrayResult()
        ));
    }

    /**
     * @Route("/manage/countries/visibility/{countryId}", name="atm_ot_manage_countries_visibility", options={"expose"=true})
     */
    public function setCountryVisibilityAction($countryId){
        $em = $this->getDoctrine()->getManager();

        $country = $em->getRepository('ATMOrderTrackerBundle:Country')->findOneById($countryId);

        $hidden = $country->getHidden();

        $country->setHidden($hidden ? false : true);
        $em->persist($country);
        $em->flush();

        return new Response($country->getHidden() ? 1 : 0);
    }

    /**
     * @Route("/gift/historical/{giftedTo}/{page}", name="atm_ot_gift_historical", options={"expose"=true},defaults={"page":1})
     */
    public function giftHistoricalAction($giftedTo, $page){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_order_tracker_config');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial u.{id,username}')
            ->from($config['user'],'u')
            ->orderBy('u.username');

        $currentDate = new DateTime();
        $currentYear = $currentDate->format('Y');
        $currentMonth = $currentDate->format('m');
        $initDate = $currentYear.'-'.$currentMonth.'-01';
        $numDays = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
        $endDate = $currentYear.'-'.$currentMonth.'-'.$numDays;

        $dates = $this->getAdminDates($initDate,$endDate);

        return $this->render('ATMOrderTrackerBundle:Admin:gift_historical.html.twig',array(
            'yearsFilter' => $dates['years'],
            'initDateTokens' => $dates['initDateTokens'],
            'endDateTokens' => $dates['endDateTokens'],
            'initDate' => $initDate,
            'endDate' => $endDate,
            'initPeriod' => $dates['initPeriod'],
            'endPeriod' => $dates['endPeriod'],
            'page' => $page,
            'users' => $qb->getQuery()->getArrayResult(),
            'userId' => null,
            'sent' => 0,
            'giftedTo' => $giftedTo
        ));
    }

    /**
     * @Route("/shipping/addresses/{page}", name="atm_ot_list_shipping_addresses",defaults={"page":1})
     */
    public function shippingAddressesAction($page){
        $em = $this->getDoctrine()->getManager();

        $qbIds = $em->createQueryBuilder();
        $qbIds
            ->select('partial sh.{id}')
            ->from('ATMOrderTrackerBundle:ShippingAddress','sh')
            ->join('sh.user','u')
            ->orderBy('u.username','DESC');

        $ids = array_map(function($i){
            return $i['id'];
        },$qbIds->getQuery()->getArrayResult());

        $shippingAddresses = $pagination = null;
        if(!empty($ids)) {


            $pagination = $this->get('knp_paginator')->paginate(
                $ids,
                $page,
                15
            );

            $ids = $pagination->getItems();

            $qb = $em->createQueryBuilder();

            $qb
                ->select('sh')
                ->addSelect('partial u.{id,username,usernameCanonical,title}')
                ->from('ATMOrderTrackerBundle:ShippingAddress','sh')
                ->join('sh.user','u')
                ->where(
                    $qb->expr()->in('sh.id',$ids)
                )
                ->orderBy('sh.approved','ASC')
                ->addOrderBy('u.username','ASC');

            $shippingAddresses = $qb->getQuery()->getArrayResult();
        }

        $currentDate = new DateTime();
        $currentYear = $currentDate->format('Y');
        $currentMonth = $currentDate->format('m');
        $initDate = $currentYear.'-'.$currentMonth.'-01';
        $numDays = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
        $endDate = $currentYear.'-'.$currentMonth.'-'.$numDays;

        $dates = $this->getAdminDates($initDate,$endDate);


        return $this->render('ATMOrderTrackerBundle:Admin:shipping_addresses.html.twig',array(
            'shipping_addresses' => $shippingAddresses,
            'page' => $page,
            'yearsFilter' => $dates['years'],
            'initDateTokens' => $dates['initDateTokens'],
            'endDateTokens' => $dates['endDateTokens'],
            'initPeriod' => $dates['initPeriod'],
            'endPeriod' => $dates['endPeriod'],
            'initDate' => $initDate,
            'endDate' => $endDate
        ));
    }

    /**
     * @Route("/edit/shipping/address/{shippingAddressId}", name="atm_ot_edit_shipping_address")
     */
    public function editShippingAddressAction($shippingAddressId){
        $em = $this->getDoctrine()->getManager();
        $shippingAddress = $em->getRepository('ATMOrderTrackerBundle:ShippingAddress')->findOneById($shippingAddressId);

        $form = $this->createForm(ShippingAddressType::class,$shippingAddress);
        $request = $this->get('request_stack')->getCurrentRequest();

        if($request->getMethod() == 'POST') {

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($shippingAddress);
                $em->flush();

                return $this->redirect($this->get('router')->generate('atm_ot_list_shipping_addresses'));
            }
        }

        return $this->render('ATMOrderTrackerBundle:Admin:edit_shipping_address.html.twig',array(
            'form' => $form->createView(),
            'shippingAddressId' => $shippingAddressId
        ));
    }

    /**
     * @Route("/delete/shipping/address/{shippingAddressId}", name="atm_ot_delete_shipping_address")
     */
    public function deleteShippingAddressAction($shippingAddressId){
        $em = $this->getDoctrine()->getManager();
        $shippingAddress = $em->getRepository('ATMOrderTrackerBundle:ShippingAddress')->findOneById($shippingAddressId);

        $em->remove($shippingAddress);
        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_ot_list_shipping_addresses'));
    }
}
