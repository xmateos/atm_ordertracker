<?php

namespace ATM\OrderTrackerBundle\Extension;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use \DateTime;

class OrderTrackerExtension extends \Twig_Extension{

    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('ATMOrderTrackerGetDateByRangeName', array($this, 'getDateByRangeName')),
            new \Twig_SimpleFunction('ATMOrderTrackerMediaHasPoints', array($this, 'mediaHasPoints')),
            new \Twig_SimpleFunction('ATMOrderTrackerGetUserPoints', array($this, 'getUserPoints')),
            new \Twig_SimpleFunction('ATMOrderTrackerCheckStock', array($this, 'checkStock')),
            new \Twig_SimpleFunction('ATMOrderTrackerIsProductFavorite', array($this, 'isProductFavorite')),
            new \Twig_SimpleFunction('ATMOrderTrackerHasShippingAddress', array($this, 'hasShippingAddress')),
            new \Twig_SimpleFunction('ATMOrderTrackerGetUserShippingAddress', array($this, 'getUserShippingAddress'))

        );
    }


    public function getDateByRangeName($range){
        $currentDate = new DateTime();
        $currentYear = $currentDate->format('Y');

        switch($range){
            case 'current_month':
                $currentMonth = $currentDate->format('m');
                $initDate = $currentYear.'-'.$currentMonth.'-01';
                $numDays = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
                $endDate = $currentYear.'-'.$currentMonth.'-'.$numDays;
                break;
            case 'last_month':
                $currentDate->modify('-1 month');
                $lastMonth = $currentDate->format('m');
                $initDate = $currentDate->format('Y').'-'.$lastMonth.'-01';
                $numDays = cal_days_in_month(CAL_GREGORIAN, $lastMonth, $currentYear);
                $endDate = $currentDate->format('Y').'-'.$lastMonth.'-'.$numDays;
                break;
            case 'current_year':
                $initDate = $currentYear.'-01-01';
                $endDate = $currentYear.'-12-31';
                break;
        }

        return array(
            'initDate' => $initDate,
            'endDate' => $endDate
        );
    }

    public function mediaHasPoints($mediaId){
        return $this->em->getRepository('ATMOrderTrackerBundle:Points')->mediaHasPoints($mediaId);
    }

    public function getUserPoints($userId){
        return $this->em->getRepository('ATMOrderTrackerBundle:Points')->getUserTotalPoints($userId);
    }

    public function checkStock($product){
        if(isset($product['stocks'])){
            $stockCount = count($product['stocks']);
            foreach($product['stocks'] as $stock){
                if($stock['stock'] <= 0){
                    $stockCount -= 1;
                }
            }

            if($stockCount > 0 ){
                return true;
            }
            return false;
        }
    }


    public function isProductFavorite($productId){
        $user = $this->security->getToken()->getUser();

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('partial f.{id}')
            ->from('ATMOrderTrackerBundle:FavoriteProduct','f')
            ->join('f.user','u','WITH',$qb->expr()->eq('u.id',$user->getId()))
            ->join('f.product','p','WITH',$qb->expr()->eq('p.id',$productId));

        return empty($qb->getQuery()->getArrayResult()) ? false : true;

    }

    public function hasShippingAddress($userId,$checkIsApproved = false){

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('partial sa.{id}')
            ->from('ATMOrderTrackerBundle:ShippingAddress','sa')
            ->join('sa.user','u','WITH',$qb->expr()->eq('u.id',$userId));

        if($checkIsApproved){
            $qb
                ->where(
                    $qb->expr()->eq('sa.approved',1)
                );
        }

        return empty($qb->getQuery()->getArrayResult()) ? false : true;
    }

    public function getUserShippingAddress($userId){
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('sa')
            ->addSelect('u')
            ->addSelect('c')
            ->from('ATMOrderTrackerBundle:ShippingAddress','sa')
            ->join('sa.user','u','WITH',$qb->expr()->eq('u.id',$userId))
            ->join('sa.country','c')
        ;

        return $qb->getQuery()->getArrayResult()[0];
    }
}