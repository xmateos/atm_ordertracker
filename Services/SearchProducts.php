<?php

namespace ATM\OrderTrackerBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use \DateTime;


class SearchProducts{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options){
        $defaultOptions = array(
            'name' => null,
            'price_range' => array(
                'min' => null,
                'max' =>null
            ),
            'date' => null,
            'sizes' => null,
            'ids' => null,
            'pagination' => null,
            'page' => 1,
            'order_by_field' => 'position',
            'order_by_direction' => 'ASC',
            'max_results' => null,
            'show_out_of_stock' => false
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial p.{id}')
            ->from('ATMOrderTrackerBundle:Product','p');

        if(!is_null($options['name'])){
            $qbIds->andWhere($qbIds->expr()->like('p.name',$qbIds->expr()->literal('%'.$options['name'].'%')));
        }

        if(!is_null($options['ids'])){
            $qbIds->andWhere($qbIds->expr()->in('p.id',$options['ids']));
        }

        if(!is_null($options['price_range']['min'])){
            $qbIds->andWhere($qbIds->expr()->gte('p.price',$options['price_range']['min']));
        }

        if(!is_null($options['price_range']['max'])){
            $qbIds->andWhere($qbIds->expr()->gte('p.price',$options['price_range']['max']));
        }

        if(!is_null($options['date'])){
            $qbIds->andWhere($qbIds->expr()->lte('p.init_date',$qbIds->expr()->literal($options['date'])));
            $qbIds->andWhere($qbIds->expr()->gte('p.end_date',$qbIds->expr()->literal($options['date'])));
        }

        if(!is_null($options['sizes'])){
            $qbIds->join('p.sizes','s','WITH',$qbIds->expr()->in('s.name',$options['sizes']));
        }

        if(!$options['show_out_of_stock']){
            $qbIds
                ->join('p.stocks','st','WITH',$qbIds->expr()->gt('st.stock',0))
                ->groupBy('p.id');
        }


        $qbIds->orderBy('p.'.$options['order_by_field'],$options['order_by_direction']);

        $pagination = null;
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }


        $products = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                    ->select('partial p.{id,name,canonical,price,tokens,creation_date,init_date,end_date,description}')
                ->addSelect('partial i.{id,path,creation_date,position,isMain}')
                ->addSelect('partial s.{id,name, position}')
                ->addSelect('partial st.{id,stock}')
                ->addSelect('partial st_size.{id,name}')
                ->from('ATMOrderTrackerBundle:Product','p')
                ->join('p.images','i')
                ->leftJoin('p.sizes','s')
                ->leftJoin('p.stocks','st')
                ->leftJoin('st.size','st_size')
                ->where($qb->expr()->in('p.id',$ids))
                ->orderBy('p.'.$options['order_by_field'],$options['order_by_direction'])
                ->addOrderBy('s.position','DESC');

            $products = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $products,
            'pagination' => $pagination
        );

    }
}