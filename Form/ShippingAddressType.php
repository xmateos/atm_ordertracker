<?php

namespace ATM\OrderTrackerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use ATM\OrderTrackerBundle\Entity\ShippingAddress;

class ShippingAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name',TextType::class,array('required'=>true,'attr'=> array('placeholder'=>'First Name')))
            ->add('last_name',TextType::class,array('required'=>true,'attr'=> array('placeholder'=>'Last Name')))
            ->add('email',EmailType::class,array('required'=> true,'attr'=> array('placeholder'=>'Email')))
            ->add('phone',TextType::class,array('required'=>true,'attr'=> array('placeholder'=>'Phone Number')))
            ->add('address',TextareaType::class,array('required'=>true,'attr'=> array('placeholder'=>'Address')))
            ->add('delivery_instructions',TextareaType::class,array('required'=>false,'attr'=> array('placeholder'=>'Delivery Instructions')))
            ->add('city',TextType::class,array('required'=>true,'attr'=> array('placeholder'=>'City')))
            ->add('state',TextType::class,array('required'=>false,'attr'=> array('placeholder'=>'State')))
            ->add('zipCode',TextType::class,array('required'=>true,'attr'=> array('placeholder'=>'Zip Code')))
            ->add('country', EntityType::class, array(
                'class' => 'ATM\OrderTrackerBundle\Entity\Country',
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('c');
                    return $qb
                            ->select('c')
                            ->addSelect('cc')
                            ->leftJoin('c.shipmentCost','cc')
                            ->where($qb->expr()->eq('c.hidden',0))
                            ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'empty_data'  => null,
                'required' => true
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShippingAddress::class,
        ));
    }

    public function getName()
    {
        return 'atmorder_tracker_bundle_shipping_address_type';
    }
}