<?php

namespace ATM\OrderTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\OrderTrackerBundle\Repository\PointsRepository")
 * @ORM\Table(name="atm_points")
 */
class Points{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    protected $media;

    /**
     * @ORM\Column(name="points", type="float")
     */
    private $points;

    /**
     * @ORM\Column(name="is_paid", type="boolean", nullable=false)
     */
    private $isPaid;

    public function __construct()
    {
        $this->creation_date = new DateTime();
        $this->isPaid = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setPoints($points)
    {
        $this->points = $points;
    }

    public function getIsPaid()
    {
        return $this->isPaid;
    }

    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
    }
}