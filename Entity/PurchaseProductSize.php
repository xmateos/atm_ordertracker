<?php

namespace ATM\OrderTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_purchase_product_size")
 */
class PurchaseProductSize{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Purchase", inversedBy="purchasedProductSizes")
     */
    protected $purchase;

    /**
     * @ORM\Column(name="product_name", type="string", length=255, nullable=false)
     */
    protected $productName;

    /**
     * @ORM\ManyToOne(targetEntity="Size")
     */
    protected $size;

    /**
     * @ORM\Column(name="price",type="decimal", precision=7, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(name="tokens",type="decimal", precision=7, scale=2)
     */
    private $tokens;

    public function __construct()
    {
        $this->price = 0;
        $this->tokens = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPurchase()
    {
        return $this->purchase;
    }

    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }

    public function getProductName()
    {
        return $this->productName;
    }

    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getTokens()
    {
        return $this->tokens;
    }

    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
    }
}