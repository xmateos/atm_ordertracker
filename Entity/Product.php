<?php

namespace ATM\OrderTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ATM\OrderTrackerBundle\Helpers\Canonicalizer;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_product")
 */
class Product{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="canonical", type="string", length=255, nullable=false)
     */
    private $canonical;

    /**
     * @ORM\Column(name="price",type="integer", nullable=false)
     */
    private $price;

    /**
     * @ORM\Column(name="tokens",type="integer", nullable=false)
     */
    private $tokens;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="init_date", type="datetime", nullable=false)
     */
    private $init_date;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $end_date;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="product", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="product", cascade={"remove"})
     */
    private $stocks;

    /**
     * @ORM\ManyToMany(targetEntity="Size")
     * @ORM\JoinTable(name="atm_products_sizes",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="size_id", referencedColumnName="id")}
     *      )
     */
    protected $sizes;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="position",type="integer", nullable=false)
     */
    private $position;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->images = new ArrayCollection();
        $this->sizes = new ArrayCollection();
        $this->position = 0;
        $this->tokens = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->canonical = Canonicalizer::canonicalize($name);
    }

    public function getCanonical()
    {
        return $this->canonical;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getInitDate()
    {
        return $this->init_date;
    }

    public function setInitDate($init_date)
    {
        $this->init_date = $init_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    public function getImages($index = null)
    {
        if(!is_null($index)){
            foreach($this->images as $image){
                if($image->getPosition() == $index){
                    return $image;
                }
            }

        }
        return $this->images;
    }

    public function getSizes()
    {
        return $this->sizes;
    }

    public function addSizes($size)
    {
        $this->sizes->add($size);
    }

    public function removeSize($size){
        $this->sizes->removeElement($size);
    }

    public function getStocks()
    {
        return $this->stocks;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getTokens()
    {
        return $this->tokens;
    }

    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
    }
}